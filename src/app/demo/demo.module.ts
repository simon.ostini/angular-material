import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MaterialModule } from "../shared/material.module";
import { FormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";

import { DemoRoutingModule } from "./demo-routing.module";
import { ButtonsComponent } from "./buttons/buttons.component";
import { FlexBoxComponent } from "./flex-box/flex-box.component";

@NgModule({
  declarations: [ButtonsComponent, FlexBoxComponent],
  imports: [
    CommonModule,
    DemoRoutingModule,
    MaterialModule,
    FormsModule,
    FlexLayoutModule,
  ],
})
export class DemoModule {}
