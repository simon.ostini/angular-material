import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { Routes, RouterModule } from "@angular/router";

import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./shared/material.module";
import { FormsModule } from "@angular/forms";
import { UserService } from "./contactmanager/services/user.service";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { HttpClientModule } from "@angular/common/http";
import { httpInterceptorProviders } from "./contactmanager/interceptors";

const routes: Routes = [
  {
    path: "contactmanager",
    loadChildren: async () =>
      await (
        await import("./contactmanager/contactmanager.module")
      ).ContactmanagerModule,
  },
  {
    path: "demo",
    loadChildren: async () =>
      await (
        await import("./demo/demo.module")
      ).DemoModule,
  },
  { path: "**", redirectTo: "contactmanager" },
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot([]),
    EffectsModule.forRoot([]),
    HttpClientModule,
  ],
  providers: [UserService, ...httpInterceptorProviders],
  bootstrap: [AppComponent],
})
export class AppModule {}
