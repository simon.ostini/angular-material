import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MaterialModule } from "../shared/material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";

import { ContactmanagerAppComponent } from "./contactmanager-app.component";
import { ToolbarComponent } from "./components/toolbar/toolbar.component";
import { MainContentComponent } from "./components/main-content/main-content.component";
import { SidenavComponent } from "./components/sidenav/sidenav.component";
import { RouterModule, Routes } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { StoreModule } from "@ngrx/store";
import { userKey } from "./components/stores/user/user.selectors";
import { userReducer } from "./components/stores/user/user.reducer";
import { UserService } from "./services/user.service";
import { EffectsModule } from "@ngrx/effects";
import { UserEffects } from "./components/stores/user/user.effects";
import { NotesComponent } from "./components/notes/notes.component";
import { NewContactDialogComponent } from "./components/new-contact-dialog/new-contact-dialog.component";

const routes: Routes = [
  {
    path: "",
    component: ContactmanagerAppComponent,
    children: [
      { path: ":id", component: MainContentComponent },
      { path: "", component: MainContentComponent },
    ],
  },
  { path: "**", redirectTo: "" },
];

@NgModule({
  declarations: [
    ContactmanagerAppComponent,
    ToolbarComponent,
    MainContentComponent,
    SidenavComponent,
    NotesComponent,
    NewContactDialogComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature(userKey, userReducer),
    EffectsModule.forFeature([UserEffects]),
  ],
  providers: [UserService],
})
export class ContactmanagerModule {}
