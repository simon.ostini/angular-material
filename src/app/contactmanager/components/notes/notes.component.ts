import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { Note } from "../../models/note";

@Component({
  selector: "app-notes",
  templateUrl: "./notes.component.html",
  styleUrls: ["./notes.component.scss"],
})
export class NotesComponent implements OnInit, AfterViewInit {
  @Input() public notes!: Note[] | undefined;
  public displayedColumns: string[] = ["position", "title", "date"];
  public dataSource!: MatTableDataSource<Note>;
  @ViewChild(MatPaginator) public paginator!: MatPaginator;
  public pageSizeOptions: number[] = [1, 2, 3];
  public pageSize: number = 2;

  constructor() {}
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<Note>(this.notes);
  }
}
