import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { User } from "../../models/user";
import { UserState } from "../stores/user/user.reducer";
import { userUserByIdSelector } from "../stores/user/user.selectors";

@Component({
  selector: "app-main-content",
  templateUrl: "./main-content.component.html",
  styleUrls: ["./main-content.component.scss"],
})
export class MainContentComponent implements OnInit {
  public user$!: Observable<User | undefined>;

  constructor(
    private _activeRoute: ActivatedRoute,
    private _userStore: Store<UserState>
  ) {}

  ngOnInit(): void {
    this._activeRoute.params.subscribe((params: Params) => {
      this.user$ = this._userStore.select(userUserByIdSelector, +params["id"]);
    });
  }
}
