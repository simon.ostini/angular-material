import { createReducer, on } from "@ngrx/store";
import { User } from "src/app/contactmanager/models/user";
import { addUser, usersLoaded, selectUser } from "./user.actions";

export interface UserState {
  users: User[];
  userId: number | undefined;
}

export const initialUserState: UserState = {
  users: [],
  userId: 1,
};

export const userReducer = createReducer(
  initialUserState,
  on(usersLoaded, (state: UserState, action: UserState) => {
    return {
      ...state,
      users: action.users,
    };
  }),
  on(selectUser, (state, action) => ({ ...state, userId: action.id })),
  on(addUser, (state, action) => ({
    ...state,
    users: [...state.users, <User>{ ...action, id: state.users.length + 1 }],
  }))
);
