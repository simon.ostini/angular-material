import { createAction, props } from "@ngrx/store";
import { User } from "src/app/contactmanager/models/user";
import { UserState } from "./user.reducer";

export const userActionPrefix = "[User]" as const;

export const getAllUsers = createAction(`${userActionPrefix} get all users`);

export const usersLoaded = createAction(
  `${userActionPrefix} users loaded`,
  props<UserState>()
);

export const selectUser = createAction(
  `${userActionPrefix} select user`,
  props<{ id: number }>()
);

export const addUser = createAction(
  `${userActionPrefix} add user`,
  props<User>()
);
