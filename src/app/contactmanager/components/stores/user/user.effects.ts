import { Injectable } from "@angular/core";
import {
  Actions,
  createEffect,
  CreateEffectMetadata,
  ofType,
} from "@ngrx/effects";
import { UserService } from "src/app/contactmanager/services/user.service";
import { getAllUsers, usersLoaded } from "./user.actions";
import { delay, map, mergeMap } from "rxjs/operators";
import { Observable } from "rxjs";
import { User } from "src/app/contactmanager/models/user";
import { UserState } from "./user.reducer";
import { TypedAction } from "@ngrx/store/src/models";

@Injectable()
export class UserEffects {
  loadUsers$: Observable<UserState & TypedAction<"[User] users loaded">> &
    CreateEffectMetadata = createEffect(() =>
    this._actions$.pipe(
      ofType(getAllUsers),
      mergeMap(() =>
        this._userService.fetchUsers().pipe(
          delay(1000),
          map((users: User[]) => usersLoaded({ users: users, userId: 1 }))
        )
      )
    )
  );

  constructor(private _actions$: Actions, private _userService: UserService) {}
}
