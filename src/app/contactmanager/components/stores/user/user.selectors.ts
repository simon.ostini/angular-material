import { createFeatureSelector, createSelector } from "@ngrx/store";
import { User } from "src/app/contactmanager/models/user";
import { UserState } from "./user.reducer";

export const userKey = "user" as const;

export const userSelector = createFeatureSelector<UserState>(userKey);

export const userUserSelector = createSelector(
  userSelector,
  (state: UserState) => state.users
);

export const userUserLengthSelector = createSelector(
  userSelector,
  (state: UserState) => state.users.length
);

export const userUserByIdSelector = createSelector(
  userSelector,
  (state: UserState, id: number) =>
    state.users.find((user: User) => user.id === id)
);
