import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from "@angular/cdk/layout";
import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { User } from "../../models/user";
import { getAllUsers } from "../stores/user/user.actions";
import { UserState } from "../stores/user/user.reducer";
import { userUserSelector } from "../stores/user/user.selectors";

@Component({
  selector: "app-sidenav",
  templateUrl: "./sidenav.component.html",
  styleUrls: ["./sidenav.component.scss"],
})
export class SidenavComponent implements OnInit {
  public isScreenSmall!: boolean;
  public users$: Observable<User[]> = this._userStore.select(userUserSelector);

  constructor(
    private _breakpointObserver: BreakpointObserver,
    private _userStore: Store<UserState>
  ) {}

  ngOnInit(): void {
    this._breakpointObserver
      .observe([Breakpoints.XSmall])
      .subscribe((state: BreakpointState) => {
        this.isScreenSmall = state.matches;
      });

    this._userStore.dispatch(getAllUsers());
  }
}
