import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import {
  MatSnackBar,
  MatSnackBarRef,
  SimpleSnackBar,
} from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { concatMap, mergeMap, switchMap, tap } from "rxjs/operators";
import { User } from "../../models/user";
import { NewContactDialogComponent } from "../new-contact-dialog/new-contact-dialog.component";
import { UserState } from "../stores/user/user.reducer";
import {
  userUserLengthSelector,
  userUserSelector,
} from "../stores/user/user.selectors";

@Component({
  selector: "app-toolbar",
  templateUrl: "./toolbar.component.html",
  styleUrls: ["./toolbar.component.scss"],
})
export class ToolbarComponent implements OnInit {
  public users$: Observable<User[]> = this._userStore.select(userUserSelector);
  @Output() toggleSidenav = new EventEmitter<void>();

  constructor(
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _userStore: Store<UserState>
  ) {}

  ngOnInit(): void {}

  onAddContactDialog(): void {
    let dialogRef = this._dialog.open(NewContactDialogComponent, {
      width: "450px",
    });

    dialogRef
      .afterClosed()
      .pipe(
        mergeMap(
          (result: any) =>
            result &&
            this._openSnackBar("Contact added", "Navigate")
              .onAction()
              .pipe(concatMap(() => of(result)))
        ),
        tap(console.log),
        mergeMap(() => this._userStore.select(userUserLengthSelector))
      )
      .subscribe((result: any) => {
        console.log("test", result);
        return this._router.navigate(["/contactmanager", result]);
      });
  }

  private _openSnackBar(
    message: string,
    action: string
  ): MatSnackBarRef<SimpleSnackBar> {
    return this._snackBar.open(message, action, {
      duration: 5000,
    });
  }
}
