import { Component, OnInit } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { User } from "../../models/user";
import { addUser } from "../stores/user/user.actions";
import { UserState } from "../stores/user/user.reducer";

@Component({
  selector: "app-new-contact-dialog",
  templateUrl: "./new-contact-dialog.component.html",
  styleUrls: ["./new-contact-dialog.component.scss"],
})
export class NewContactDialogComponent implements OnInit {
  public user!: User;
  public avatars = ["svg-1", "svg-2", "svg-3", "svg-4"] as const;
  public name = new FormControl("", [Validators.required]);

  constructor(
    private _dialogRef: MatDialogRef<NewContactDialogComponent>,
    private _userStore: Store<UserState>
  ) {}

  ngOnInit(): void {
    this.user = new User();
  }

  save(): void {
    this.user.name = this.name.value;
    this._dialogRef.close(this.user);
    this._userStore.dispatch(addUser(this.user));
  }

  dismiss(): void {
    this._dialogRef.close(null);
  }

  getErrorMessage(): string {
    return this.name.hasError("required") ? "You must enter a name" : "";
  }
}
