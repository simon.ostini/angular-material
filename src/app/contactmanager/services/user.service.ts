import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { User } from "../models/user";

@Injectable({
  providedIn: "root",
})
export class UserService {
  private _APIBaseUrl = "https://angular-material-api.azurewebsites.net";
  constructor(private _http: HttpClient) {}

  public fetchUsers(): Observable<User[]> {
    return this._http.get<User[]>(`${this._APIBaseUrl}/users`);
  }
}
