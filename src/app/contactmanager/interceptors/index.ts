import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { Provider } from "@angular/core";
import { LoggerInterceptor } from "./logger/logger.interceptor";

export const httpInterceptorProviders: Provider[] = [
  { provide: HTTP_INTERCEPTORS, useClass: LoggerInterceptor, multi: true },
];
